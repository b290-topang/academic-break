//Variables 
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");





// Activity

/*txtFirstName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = txtFirstName.value + " "
})

txtLastName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = txtLastName.value
})

*/
txtFirstName.addEventListener("keyup", printFullName);
txtLastName.addEventListener("keyup", printFullName);

function printFullName(event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}