//Reading List Activity

//1. Declare a function that will be named as <shipItem> that will have a parameter for <item>, <weight>, and <location>.
	function shipItem (item, weight, location) {
		let shippingFee = 0;
		let weightFee = 0;

		//2. Set a condition for shipping fee:
		//If-else statement
		if (weight <= 0) {
			return "Please enter a valid of your item.";

		} else if (weight >= 1 && weight <= 3 ) {
			shippingFee += 150;

		} else if (weight >= 4 && weight <= 5) {
			shippingFee += 280;

		} else if (weight >= 6 && weight <= 8) {
			shippingFee += 340;


		} else if (weight >= 7 && weight <= 10) {
			shippingFee += 410;


		} else if (weight > 11) {
			shippingFee += 560;


		} else {
			return "Please enter the weight of your item.";

		}

		//3. Set a switch case statement that will add charge according to the location:
		//Switch Statement
		location = location.toLowerCase();
		switch (location) {
			case 'local':
				shippingFee += 0;
				break;
			case 'international':
				shippingFee += 250;
				break;
			default: 
				return `Please enter your location. (Local or International)`;
				break;
		}

		//4. Compute the total shipping fee.
			// Used (+=) operating with the variable shippingFee

		//5. Return a message:
		return `The total shipping fee for an ${item} that will ship ${location} is ${shippingFee}.`

		//6. Invoke a function via console.
		//Examples
			// shipItem("Tissue", 3, "Local")
			// 'The total shipping fee for an Tissue that will ship local is 150.'
				// weight: 150 + location: 0 = 150

			// shipItem("Chair", 15, "International")
			// 'The total shipping fee for an Chair that will ship international is 810.'
				// weight: 560 + location: 250 = 810
	}