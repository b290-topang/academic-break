//Reading List Activity

//1. Create an object for <Product> using object constructor that will have the following properties
/*
	<name> - String
	<price> - Number
	<quantity> - Number
*/
	function Product(name, price, quantity) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

//2. Create an object for <Customer> using object constructor that will have the following properties 
/*
	<name> - String
	<cart> - Arry of products
	<addToCart> - method with a parameter for product that will be added
	<removetoCart> - method with a paramter for product that will be removed
*/
	function Customer(name) {
		this.name = name,
		this.cart = [],

		//3. In <addToCart> method, use an array method that will add the product to the array cart. Return a message: "<product>" is added to cart.
		this.addToCart = function(item) {
			this.cart.push(item);
			return `${item.name} is added to cart.`;
		}

		//4. In <removeToCart> method, use array method that will remove a specific proudct from the array cart. Return a message: "<product>" is removed from the cart.
		this.removeToCart = function (item) {
			const index = this.cart.indexOf(item);
			this.cart.splice(index, 1);
			return `${item.name} is removed from the cart.`;
		}
	}

	//5. Instantiate a new product and new customer
		//new product syntax: let x = new Product(name, price, quantity);	
		let Tissue = new Product("Tissue", 8, 3);
		let Alcohol = new Product("Alcohol", 16, 7);
		let Oreo = new Product("Oreo", 1, 12);

		//new customer syntax: let y = new Customer(name);
		let John = new Customer("John Smith");
		let Jane = new Customer("Jane Doe");

	//6. Invoke the methods of <customer> via console.
		//Example invocations
			//John.addToCart(Tissue); -to add the product "Tissue" in the Customer.cart array.
			//John.addToCart(Alcohol); -to add the product "Alcohol" in the Customer.cart array.
			//John.removeToCart(Tissue); -to remove the product "Tissue" in the Customer.cart array.

			//Jane.addToCart(Oreo); -to add the product "Tissue" in the Customer.cart array.

